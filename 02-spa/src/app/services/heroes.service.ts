import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class HeroesService {

    private heroes:Heroe[]=[
        {
            nombre: "Dani",
            bio: "Se dice que, desde que tiene una mancha negra en el brazo le dieron los poderes de ser capaz de hacerse 3 cuadrakills en una partida pero, la maldición de que siempre le roban la penta.",
            img: "assets/img/Dani.jpeg",
            nickname:"Pan Trenza",
            aparicion: "2000-10-25",
            casa:"DSS"
          },
          {
            nombre: "Sotoro",
            bio: "Cuenta la leyenda que Sotoro es un gigante que juega acostao y siempre tiene sueño y para que no lo flameen por jugar mal o por que se duerme en la partida se convirtió en OTP Yummi.",
            img: "assets/img/yo.jpeg",
            nickname:"Pan de Leche",
            aparicion: "2001-04-19",
            casa:"DSS"
          },
          {
            nombre: "Meli-chan",
            bio: "Es una Diosa del olimpo pero en el fondo quiere ser una zorra y por eso maineó la mujer zorro de nueve colas, Ahri. Como la E de Ahri tiene el poder de hechizar y encantar tanto a hombres como a mujeres, pero su maldición es atraer a puro tóxicos unu.",
            img: "assets/img/Melita.jpeg",
            nickname:"Takumichan",
            aparicion: "2002-03-28",
            casa: "DSS"
          },
          {
            nombre: "Isaac",
            bio: "La habilidad que más usa este héroe es la de escribir como el orto, juega bien cuando se lo propone pero trolea por voluntad propia, se enoja mucho mucho cuando le tocan los minions. Tiene el poder del crecimiento de patillas hasta parecersea Antonio Nariño Y NO ES CAPAZ DE TOMAR UNA FOTO EN FORMATO 9:16.",
            img: "assets/img/Isaa.jpeg",
            nickname:"Pan Trillos",
            aparicion: "2001-03-18",
            casa:"DSS"
          },
          {
            nombre: "Cami",
            bio: "Tiene una identidad secreta, de día es Camilo Moreno pero de noche es Andres Pinto, lo conocemos de animes como: Camilo contra el mundo. En donde da su opinión y todos están en contra. Su poder mas caracteristico es hacer enojar a Jean (spoiler los dos se quieren en secreto).",
            img: "assets/img/Cami.jpeg",
            nickname:"UnU",
            aparicion: "2001-03-06",
            casa: "DSS"
          },

    ];

    constructor(){
        console.log("Servicio listo para usar aaaaah");
    }

    getHeroes():Heroe[]{
        return this.heroes;
    }
    getHeroe(idx:number){
        return this.heroes[idx];
    }
}
export interface Heroe{
    nombre: string;
    bio: string;
    img: string;
    nickname: string;
    aparicion: string;
    casa: string;
  }