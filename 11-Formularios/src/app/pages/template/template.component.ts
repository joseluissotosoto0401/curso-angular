import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PaisService } from 'src/app/services/pais.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  usuario={
    nombre: 'Jose',
    apellido:'Soto',
    email: 'jls@gm.com',
    pais: 'COL',
    sexo: 'M'
  }

  paises: any[] = [];
  constructor(private paisServices:PaisService) { }

  ngOnInit(): void {

    this.paisServices.getPaises().
      subscribe( paises=> {
        this.paises = paises;
        this.paises.unshift({
          nombre: '[Seleccione un pais]',
          codigo: ''
        })

        //console.log(this.paises)
      })
  }

  guardar(forma:NgForm){
    console.log(forma);
    if ( forma.invalid ){
      //Para cuando le dan guardar sin nada indique la validacion 
      Object.values( forma.controls).forEach( control => {
        control.markAsTouched();
      })  
    }
    console.log(forma.value)
  }
}
