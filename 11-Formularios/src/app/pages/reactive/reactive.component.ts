import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/services/validadores.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  forma!: FormGroup;

  constructor( private fb:FormBuilder,
               private validadores:ValidadoresService) { 
    this.crearFormulario();
    this.cargarDataAlFormulario();
    this.crearListeners();

  }

  ngOnInit(): void {
  }

  get pasatiempos(){
    return this.forma.get('pasatiempos') as FormArray;
  }

  get nombreNoValido(){
  
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched
  
  }

 get apellidoNoValido(){

  return this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.touched

 }

 get correoNoValido(){

  return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched

 }

 get usuarioNoValido(){

  return this.forma.get('usuario')?.invalid && this.forma.get('usuario')?.touched

 }

 get ciudadNoValida(){

  return this.forma.get('direccion.ciudad')?.invalid && this.forma.get('direccion.ciudad')?.touched

 }
 
 get principalNoValida(){

  return this.forma.get('direccion.principal')?.invalid && this.forma.get('direccion.principal')?.touched

 }

 get secundariaNoValida(){

  return this.forma.get('direccion.secundaria')?.invalid && this.forma.get('direccion.secundaria')?.touched

 }

 get numeralNoValido(){

  return this.forma.get('direccion.numeral')?.invalid && this.forma.get('direccion.numeral')?.touched

 }

 get pass1NoValido(){
  return this.forma.get('pass1').invalid && this.forma.get('pass1').touched;
 }

 get pass2NoValido(){
  const pass1 = this.forma.get('pass1').value;
  const pass2 = this.forma.get('pass2').value;
  //forma larga
  /*if (pass1 === pass2 ){
    return false;
  }else return true;*/
  return ( pass1 === pass2 ) ? false : true;

 }

  crearFormulario(){

    this.forma = this.fb.group({
      nombre   : ['', [ Validators.required, Validators.minLength(3), this.validadores.noJosel] ],
      apellido : ['', [ Validators.required, Validators.minLength(3)] ],      
      correo   : ['', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')] ],
      usuario : ['', , this.validadores.existeUsuario],
      pass1 : ['', [ Validators.required ]],
      pass2 : ['', [ Validators.required ]],
      direccion: this.fb.group({
        ciudad  :   ['',[ Validators.required, Validators.minLength(3)]],
        principal:  ['',[ Validators.required ]],
        secundaria: ['',[ Validators.required, Validators.pattern('[1-9]+[0-9]*$')]],
        numeral:    ['',[ Validators.required, Validators.pattern('[1-9]+[0-9]*$')]],
      }),
      //arreglos de formControl-formArray
      pasatiempos: this.fb.array([]),
      //validadores: ['',sincronos, asincronos]
    },{
      validators: this.validadores.passwordsIguales( 'pass1', 'pass2')
    });
  }

  crearListeners(){
    //this.forma.valueChanges.subscribe( valor =>{
    //  console.log(valor);
    //});

    //this.forma.statusChanges.subscribe( status => console.log({status}) )
    //Para todo el formulario

    this.forma.get('nombre').valueChanges.subscribe( console.log )
  }

  cargarDataAlFormulario(){
    //no permite campos sin llenar
    this.forma.reset(
      {
        nombre: 'Jose',
        apellido: 'Soto',
        correo: 'jls@gmail.com',
        usuario:'',
        direccion: {
          ciudad: 'Piedecuesta',
          principal: 'Carrera 13',
          secundaria: '5',
          numeral: '19'
        },
        pass1: 123,
        pass2: 123
      }
    );

    //['comer', 'mimir'].forEach( valor=> this.pasatiempos.push(this.fb.control(valor)) );
    //cargar data a un formArray
  }

  agregarPasatiempo(){
    this.pasatiempos.push( this.fb.control('') );
  }

  borrarPasatiempos(i:number){
    this.pasatiempos.removeAt(i);
  }

  guardar(){

    console.log(this.forma);

    if ( this.forma.invalid ){
      //Para cuando le dan guardar sin nada indique la validacion 
      return Object.values( this.forma.controls ).forEach( control => {

        if( control instanceof FormGroup){
          Object.values( control.controls ).forEach( control => control.markAsTouched());
        }else{

        control.markAsTouched();
        }
      })  
    }
    
    console.log(this.forma.value)

    //posteo del formulario

    this.forma.reset();

    //resetea todo y permite campos sin llenar
  }
}
