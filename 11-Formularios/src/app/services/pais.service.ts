import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class PaisService {

  constructor(private http: HttpClient) { 
 
  }

  getPaises(){
    return this.http.get('https://restcountries.com/v3.1/lang/spa')
    .pipe( 
      map( (paises) => {
        return Object.values(paises).map( pais =>( {nombre: pais.name.nativeName.spa.common, bandera: pais.flag,codigo: pais.cioc}))
         
      })
     );
  }
}
