import { Data } from "@angular/router";
import { ListaItem } from "./Lista-item.model";

export class Lista{
    id: number;
    titulo: string;
    creadaEn: Data;
    terminadaEn: Data;
    terminada: boolean;
    items: ListaItem[];

    constructor(titulo:string){
        this.titulo=titulo;
        this.creadaEn= new Date();
        this.terminada=false;
        this.items=[];
        this.id= new Date().getTime();
    }
}