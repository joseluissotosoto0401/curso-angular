import { Component } from '@angular/core';

@Component({
    selector: 'app-body',
    templateUrl: './body.component.html'
})
export class BodyComponent{
    
    mostrar=true;
    frase: any ={
        mensaje: 'Ante la duda la mas tetuda',
        autor: 'Yotas'
    };

    personajes: string[] = ['spiderman','venon', 'Dr. Optopus']

}