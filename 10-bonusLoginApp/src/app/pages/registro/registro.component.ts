import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario: UsuarioModel;
  recordarme: false;
  constructor( private auth: AuthService,
               private router:Router ) { }

  ngOnInit() { 
    this.usuario = new UsuarioModel();

  }
  onSubmit( form: NgForm){

    if ( form.invalid ) { return;}

    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'espere por favor...',
      width: 600,
      padding: '3em',
      background: '#fff',
      backdrop: `
        rgb(29,73,115)
        url('./assets/images/nyan-cat.gif')
        left top
        no-repeat
      ` 
    });
    Swal.showLoading();

    this.auth.nuevoUsusario(this.usuario)
      .subscribe( resp => {

        console.log(resp);
        Swal.close();
        this.router.navigateByUrl('/home')
        if ( this.recordarme ){
          localStorage.setItem('email', this.usuario.email)
        }

      }, (err) => {
        console.log(err.error.error.message)
        Swal.fire({
          icon: 'error',
          title: 'Error al autenticar',
          text: err.error.error.message,
          width: 600,
          padding: '3em',
          background: '#fff',
          backdrop: `
            rgb(29,73,115)
            url('./assets/images/nyan-cat.gif')
            left top
            no-repeat
          `
        })
      })

    /*     console.log('Formulario enviado ')
    console.log(this.usuario)
    console.log(form) */
  }

}
