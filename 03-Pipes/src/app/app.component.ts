import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre : string = "Jose luis"
  nombre2 : string = "jOSe lUiS soTO"
  arreglo=[1,2,3,4,5,6,7,8,9,10];
  PI:number= Math.PI;
  porcentaje: number= 0.8526
  salario: number= 3100;
  fecha: Date = new Date();
  activar: boolean = true;
  idioma: string = 'fr';
  videoUrl: string ='https://www.youtube.com/embed/BmJ4QSR5SMo';
  valorPromesa= new Promise<string>((resolve) => {

    setTimeout(()=>{
      resolve('llegó la data')
    }, 4500);
    
  })

  heroe={
    nombre: 'Logan',
    clave: 'wolverine',
    edad: 500,
    direccion:{
      carrera: 13,
      numero: '5-19'
    }
  }


}
