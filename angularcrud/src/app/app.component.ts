import { Component } from '@angular/core';
import { EmptyError } from 'rxjs';

import {Empleados} from './models/empleados';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  empleadosArray: Empleados[] =[
    {id: 1, name: "Alita", country: "Colombia"},
    {id: 2, name: "Jose", country: "Colombia"},
    {id: 3, name: "Putin", country: "Russia"}
  ];

  SelectedEmpleados: Empleados = new Empleados();

  OpenForEdit(empleados:Empleados){
    this.SelectedEmpleados = empleados;
  }

  addOrEdit(){
    if (this.SelectedEmpleados.id == 0){
      this.SelectedEmpleados.id = this.empleadosArray.length +1;
      this.empleadosArray.push(this.SelectedEmpleados);
    }
   
    this.SelectedEmpleados = new Empleados();
  };

  delete(){
    if(confirm('¿Estás seguro que deseas eliminarlo?')){
      this.empleadosArray = this.empleadosArray.filter(x=>x!= this.SelectedEmpleados);
      this.SelectedEmpleados = new Empleados
    }

  }
 
}
