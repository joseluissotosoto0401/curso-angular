import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
 
  constructor( private http:HttpClient ) { 
    console.log("spotify listo")
  }
  getQuery ( query:any ){
    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization':'Bearer BQAyB0anw_Np8xqWCZyawg05BTbkBHmRIDWHV9wIDySl0AzvisSB35U52OJj0nzGJLRCqvsw3e8c7j-pUHQ'
      })
      return this.http.get(url, { headers });
  }



  getNewReleases(){

    //const headers = new HttpHeaders({
    //'Authorization':'Bearer BQAdVWZJ0qUCA_JIIWukGX901MOV8qYOh9otGXOO3Sv1JSZxnbQ9R75a30E2sH6fl8AthFcjqEqwXS2LwxU'
    //})

    return this.getQuery('browse/new-releases')
            .pipe( map( data => data["albums"].items));
  }

  getArtistas(termino: string){
    return this.getQuery(`search?q=${ termino }s&type=artist&limit=15`)
            .pipe( map( data => data["artists"].items));
  }

  getArtista(id: string){
    return this.getQuery(`artists/${ id }`)
            //.pipe( map( data => data["artists"].items));
  }
  getTracks(id: string){
    return this.getQuery(`artists/${ id }/top-tracks?market=us`)
            .pipe( map( data => data["tracks"]));
  }
}
