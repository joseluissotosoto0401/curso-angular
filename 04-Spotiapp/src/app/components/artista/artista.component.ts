import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';


@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent {

  artista: any = {};
  tracks: any = {};
  loadingAr: boolean;
  constructor( private router:ActivatedRoute,
               private spotify:SpotifyService) {
    this.loadingAr = true; 
    this.router.params.subscribe( params => {
      this.getArtista(params['id']);
      this.getTracks(params['id']);
    })
  }

  getArtista(id:string){
    this.loadingAr = true; 
    this.spotify.getArtista( id ).subscribe(
      artista=>{
        console.log(artista);
        this.artista= artista;
        this.loadingAr = false;
        
      }
    )
  }
  getTracks(id:string){
    this.loadingAr = true; 
    this.spotify.getTracks( id ).subscribe(
      tracks=>{
        console.log(tracks);
        this.tracks= tracks;
        this.loadingAr = false;
        
      }
    )
  }

}
