import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  nuevasCanciones: any[]=[];
  loading: boolean;
  error: boolean;
  menError: string;
  constructor( private spotify: SpotifyService) { 
    this.loading = true;
    this.error = false;
    this.spotify.getNewReleases()
      .subscribe((data: any)=>{
       console.log(data);
       this.nuevasCanciones=data;
       this.loading = false;
     }, ( errorSe ) =>{
       this.loading=false; 
       this.error=true;
       this.menError= errorSe.error.error.message;
        
     } );
      
  }

}
